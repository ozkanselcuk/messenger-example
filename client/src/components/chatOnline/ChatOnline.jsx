import React, { useState, useEffect } from "react";
import "./chatOnline.css";
import axios from "axios";

const ChatOnline = ({ onlineUsers, setCurrentChat, currentId }) => {
  const [friends, setFriends] = useState([]);
  const [onlineFriends, setOnlineFriends] = useState([]);

  useEffect(() => {
    const getFriends = async () => {
      try {
        const response = await axios.get("/users/getAll");

        setFriends(response.data);
      } catch (err) {
        console.log(err);
      }
    };

    getFriends();
  }, [currentId]);

  const handleConversationStart = async (user) => {
    try {
      const response = await axios.get(`/conversations/find/${currentId}/${user._id}`);
      setCurrentChat(response.data);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className="chatOnline">
      {friends.map((friend) => (
        <div
          className="chatOnlineFriend"
          key={friends._id}
          onClick={() => handleConversationStart(friend)}
        >
          <div className="chatOnlineImgContainer">
            <img
              className="chatOnlineImg"
              src={
                friend?.profilePicture === ""
                  ? "https://images.unsplash.com/photo-1649223316113-3d1be69fe50b?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80"
                  : friend.profilePicture
              }
              alt=""
            />
            <div className="chatOnlineBadge"></div>
          </div>
          <span className="chatOnlineName">{friend?.username}</span>
        </div>
      ))}
    </div>
  );
};

export default ChatOnline;
