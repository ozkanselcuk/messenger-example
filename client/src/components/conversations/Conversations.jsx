import axios from "axios";
import React, { useEffect, useState } from "react";
import "./conversations.css";

const Conversations = ({ conversation, currentUser }) => {
  const [user, setUser] = useState(null);
  const defaultProfilePicture = process.env.REACT_APP_PUBLIC_FOLDER;

  useEffect(() => {
    const friendId = conversation.members.find((m) => m !== currentUser._id);

    const getUser = async () => {
      try {
        const response = await axios.get("/users?userId=" + friendId);
        setUser(response.data);
      } catch (err) {
        console.log(err);
      }
    };

    getUser();
  }, [currentUser, conversation]);

  return (
    <div className="conversations">
        <>
          <img
            className="conversationImg"
            src={
              user?.profilePicture
                ? user?.profilePicture
                : defaultProfilePicture + "person/noAvatar.png"
            }
            alt=""
          />
          <span className="conversationName">{user?.username}</span>
        </>
    </div>
  );
};

export default Conversations;
